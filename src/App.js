import React, { Component } from 'react';
import './App.css';

import { ProjectListContainer } from './components/ProjectListContainer/ProjectListContainer'
import { ProjectInformation } from './components/ProjectInformation/ProjectInformation'
import { getContributorsForProject } from './utils/github'
class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      currentProject: null
    }
  }

  handleCurrentProjectChange = (project) => {
    const { currentProject } = this.state
    if (currentProject && currentProject.id === project.id) {
      return
    }
    console.log(project)
    getContributorsForProject(project).then(contributors => {
      this.setState({
        currentProject: project,
        contributors,
      })
    })
  }

  render() {
    const { currentProject, contributors } = this.state
    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-3">
            <h1 className="site-title">Facebook</h1>
            <ProjectListContainer handleCurrentProjectChange={this.handleCurrentProjectChange}/>
          </div>
          <div className="col-sm-9">
            <h1>Project Information</h1>
            <ProjectInformation project={currentProject} contributors={contributors}/>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
