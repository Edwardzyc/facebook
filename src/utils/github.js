
function fetchClient(url) {
  return fetch(url, { 
    headers: { 
      "Accept": "application/vnd.github.inertia-preview+json",
    } 
  })
}

export function getFacebookRepositories(url='https://api.github.com/organizations/69631/repos', data=[]) {
  const nextRegex = /<(\S+)>; rel="next"/
  
  return fetchClient(url).then(resp => {
    const linkHeader = resp.headers.get('Link')
    const nextLinkString = linkHeader.split(',').map(el => el.trim()).find(el => {
      return nextRegex.test(el)
    })
    if (!nextLinkString) {
      return Promise.all(data).then(projectArray => {
        return projectArray.flat().sort(function(a,b) {
          if (a.watchers > b.watchers) {
            return -1
          } else if (a.watchers === b.watchers) {
            return 0
          } else {
            return 1
          }
        })
      })
    } else {
      const nextLink = nextRegex.exec(nextLinkString)[1]
      return getFacebookRepositories(nextLink, [...data, resp.json()])
    }
  })
}

export function getContributorsForProject(project) {
  return fetchClient(project.contributors_url).then(resp => resp.json())
}