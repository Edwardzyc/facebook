import React from 'react';
import './ProjectList.css';

export function ProjectList(props) {
  const projects = props.projects.map(project => {
    return (
      <li 
        onClick={() => props.handleCurrentProjectChange(project)} 
        key={project.id} 
        className="list-group-item list-group-item-action"
      >
        {project.full_name}
      </li>
    )
  })
  return (
    <ul className="list-group">
      { projects }
    </ul>
  )
}