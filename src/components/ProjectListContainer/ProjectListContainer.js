import React from 'react';

import { ProjectList } from '../ProjectList/ProjectList'
import { getFacebookRepositories } from '../../utils/github'

export class ProjectListContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      projects: []
    }
  }



  componentDidMount() {
    getFacebookRepositories().then(repos => {
      this.setState({
        projects: repos
      })
    })
  }

  render() {
    const { projects } = this.state
    return (
      <React.Fragment>
        <ProjectList handleCurrentProjectChange={this.props.handleCurrentProjectChange} projects={projects}/>
      </React.Fragment>
    )
  }
}