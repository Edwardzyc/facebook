import React from 'react';

export function ProjectInformation(props) {
  if (props.project) {
    const contributors = props.contributors.map(c => {
      return <div key={c.id}>{c.login}</div>
    })
    return (
      <div>
      { contributors }
      </div>
    )
  } else {
    return "Please select a project"
  }
}